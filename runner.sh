# cmake
mkdir -p bin
cd bin
cmake ..

# build
make
cd ..

# run
bin/sandbox tests/test.xml