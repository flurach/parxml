#include "parxml.h"

struct buffer_t {
	unsigned int length;
	char *data;
};

struct buffer_t read_file(char *file_path)
{
	FILE *file = fopen(file_path, "r");
	assert(file);

	fseek(file, 0, SEEK_END);
	unsigned int size = ftell(file);
	rewind(file);

	char *data = malloc(size + 1);
	fread(data, 1, size, file);

	fclose(file);
	return (struct buffer_t){size, data};
}

int main(int argc, char **argv)
{
	if (argc != 2)
		return 0;

	struct buffer_t buffer = read_file(argv[1]);
	struct parxml_parser_t parser;
	parser = parxml_parser(buffer.data, buffer.length);

	struct parxml_node_t node = parxml_parser_next_node(&parser);
	struct parxml_node_t drinks = parxml_node_search_class(&node, "drink");

	unsigned int i;
        for (i = 0; i < drinks.children_count; i++)
                parxml_node_print(drinks.children + i);

	parxml_node_free(node);
	parxml_parser_free(parser);
	return 0;
}