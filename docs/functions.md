# Functions list

> Note: Functions that are not listed here but exist in code shouldn't be used. Especially static function's usage is not allowed.

> Also, if not mentioned, return value of a function should not be freed by any function.

## Creating parser and fetching nodes
``` C
/*
 * Creates a new parser from a string.
 *
 * Should be freed with parxml_parser_free
 */

struct parxml_parser_t parxml_parser(
        char *source,
        unsigned int length
);
```

``` C
/*
 * Fetches the next node from parser
 *
 * Returns PARXML_TOKENTYPE_ENDOFFILE if next node doesn't exist
 * You can check it by node.token.type
 *
 * Should be freed with parxml_node_free
 */

struct parxml_node_t parxml_parser_next_node(
        struct parxml_parser_t *parser
);
```

## Searching in nodes
``` C
/*
 * Searches for field 'id' in children (doesn't include 'self')
 * Returns a single element reference
 */

struct parxml_node_t *parxml_node_search_id(
        struct parxml_node_t *self,
        char *id
);
```

``` C
/*
 * Searches for 'class' in children's class lists
 *
 * Returns a single node which has result as it's 'children'.
 * You can iterate over them by:
 *
 * unsigned int i;
 * for (i = 0; i < list.children_count; i++)
 *      parxml_node_print(list.children[i]);
 */

struct parxml_node_t parxml_node_search_class(
	struct parxml_node_t *self,
	char *class
);
```

``` C
/*
 * Searches for 'tag' in children's tag names
 *
 * Returns the same thing as the function above
 */

struct parxml_node_t parxml_node_search_tag(
	struct parxml_node_t *self,
	char *tag
);
```