# Simple example

We will try to get elements that has class 'drink' in a XML document.

``` C
#include "parxml.h"

char *source;
unsigned int source_length;

/* Code to get source from somewhere */
...

int main()
{
        /* Create parser from source */
        struct parxml_parser_t parser
                = parxml_parser(source, source_length);

        /* Fetch next node */
        struct parxml_node_t node
                = parxml_parser_next_node(&parser);

        /* Search for 'drinks' */
        struct parxml_node_t drinks
                = parxml_node_search_class(&node, "drink");

        /* Iterate over items and print them */
        unsigned int i;
        for (i = 0; i < drinks.children_count; i++)
                parxml_node_print(drinks.children + i);

        /* Free resources and halt */
        parxml_node_free(node);
        parxml_parser_free(parser);
        return 0;
}

/*
 * Result:
 *
 * <item class="drink">
 *      Milk
 * </item>
 * <item class="drink">
 *      Coke
 * </item>
 * <item class="drink">
 *      Beer
 * </item>
 */
```

Notice, that we didn't `parxml_node_free(drinks)`. That's because we don't copy nodes when searching, rather, we take references for them.

Since, we did `parxml_node_free(node)` and `drinks` are in that node, it gets freed too.