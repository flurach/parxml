#ifndef parxml_h
#define parxml_h

/* c libs */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

/* hashmap */
struct parxml_hashmap_pair_t {
	char *key;
	char *value;
};
struct parxml_hashmap_t {
	unsigned int count;
	struct parxml_hashmap_pair_t *store;
};
struct parxml_hashmap_t parxml_hashmap(unsigned int capacity);
void parxml_hashmap_free(struct parxml_hashmap_t *self);
void parxml_hashmap_set(
	struct parxml_hashmap_t *self,
	char *key,
	char *value
);
char *parxml_hashmap_get(
	struct parxml_hashmap_t *self,
	char *key
);

/* token */
#define PARXML_TOKENTYPES                       \
	X(PARXML_TOKENTYPE_ENDOFFILE)           \
	X(PARXML_TOKENTYPE_LESSERTHAN)          \
	X(PARXML_TOKENTYPE_LESSERTHAN_SLASH)    \
	X(PARXML_TOKENTYPE_GREATERTHAN)         \
	X(PARXML_TOKENTYPE_EQUAL)               \
	X(PARXML_TOKENTYPE_TAG_START)           \
	X(PARXML_TOKENTYPE_TAG_END)             \
	X(PARXML_TOKENTYPE_WORD)                \
	X(PARXML_TOKENTYPE_STRING)
const char *parxml_tokentype_t[] = {
	#define X(value) #value,
		PARXML_TOKENTYPES
	#undef X
};
enum parxml_tokentype_t {
	#define X(value) value,
		PARXML_TOKENTYPES
	#undef X
};
struct parxml_token_t {
	enum parxml_tokentype_t type;
	char *value;
};
void parxml_token_print(struct parxml_token_t token);
void parxml_token_free(struct parxml_token_t token);
bool parxml_token_match(
	struct parxml_token_t first,
	struct parxml_token_t second
);

/* node */
struct parxml_node_t {
	unsigned int capacity;
	unsigned int classes_count;
	unsigned int children_count;

	char *id;
	char **classes;
	struct parxml_node_t *children;
	struct parxml_token_t token;
	struct parxml_hashmap_t attributes;
};
struct parxml_node_t parxml_node(
	struct parxml_token_t token,
	unsigned int capacity
);
void parxml_node_free(struct parxml_node_t self);
void parxml_node_append_child(
	struct parxml_node_t *self,
	struct parxml_node_t child
);
void parxml_node_print(struct parxml_node_t *self);
struct parxml_node_t *parxml_node_search_id(
	struct parxml_node_t *self,
	char *id
);
struct parxml_node_t parxml_node_search_class(
	struct parxml_node_t *self,
	char *class
);
struct parxml_node_t parxml_node_search_tag(
	struct parxml_node_t *self,
	char *tag
);
bool parxml_node_has_class(struct parxml_node_t *self, char *class);
void parxml_node_add_class(struct parxml_node_t *self, char *class);
void parxml_node_remove_class(struct parxml_node_t *self, char *class);
void parxml_node_toggle_class(struct parxml_node_t *self, char *class);

/* parser */
struct parxml_parser_t {
	unsigned int length;
	char *source;
	char *iter;
};
struct parxml_parser_t parxml_parser(char *source, unsigned int length);
void parxml_parser_free(struct parxml_parser_t parser);

struct parxml_token_t parxml_parser_next_token(struct parxml_parser_t *parser);
struct parxml_token_t parxml_parser_peek_token(struct parxml_parser_t *parser);

struct parxml_node_t parxml_parser_next_node(struct parxml_parser_t *parser);
struct parxml_node_t parxml_parser_parse_tag(struct parxml_parser_t *parser);

#endif