#include "parxml.h"

struct parxml_hashmap_t parxml_hashmap(unsigned int capacity)
{
	struct parxml_hashmap_pair_t *store;
	store = calloc(capacity, sizeof(struct parxml_hashmap_pair_t));

	return (struct parxml_hashmap_t){
		.count = 0,
		.store = store
	};
}

void parxml_hashmap_free(struct parxml_hashmap_t *self)
{
	unsigned int i;
	for (i = 0; i < self->count; i++) {
		if (self->store[i].key != NULL) {
			free(self->store[i].key);
			free(self->store[i].value);
		}
	}
	free(self->store);
}

void parxml_hashmap_set(
	struct parxml_hashmap_t *self,
	char *key,
	char *value
)
{
	unsigned int i;
	for (i = 0; i < self->count; i++) {
		if (!strcmp(self->store[i].key, key)) {
			free(self->store[i].value);
			self->store[i].value = strdup(value);
			return;
		}
	}

	struct parxml_hashmap_pair_t pair = {strdup(key), strdup(value)};
	self->store[self->count++] = pair;
}

char *parxml_hashmap_get(
	struct parxml_hashmap_t *self,
	char *key
)
{
	unsigned int i;
	for (i = 0; i < self->count; i++) {
		if (!strcmp(self->store[i].key, key))
			return strdup(self->store[i].value);
	}

	return NULL;
}

void parxml_token_print(struct parxml_token_t token)
{
	printf(
		"(%s, %s)",
		parxml_tokentype_t[token.type],
		token.value
	);
}

void parxml_token_free(struct parxml_token_t token)
{
	if (token.type >= PARXML_TOKENTYPE_WORD)
		free(token.value);
}

bool parxml_token_match(
	struct parxml_token_t first,
	struct parxml_token_t second
)
{
	return
		first.type == PARXML_TOKENTYPE_TAG_START
		&&
		second.type == PARXML_TOKENTYPE_TAG_END
		&&
		!strcmp(first.value, second.value);
}

struct parxml_node_t parxml_node(
	struct parxml_token_t token,
	unsigned int capacity
)
{
	struct parxml_node_t *children;
	children = calloc(capacity, sizeof(struct parxml_node_t));
	char **classes = calloc(capacity, sizeof(char*));

	return (struct parxml_node_t){
		.capacity = capacity,
		.classes_count = 0,
		.children_count = 0,

		.id = NULL,
		.classes = classes,
		.children = children,
		.token = token,
		.attributes = parxml_hashmap(capacity)
	};
}

void parxml_node_free(struct parxml_node_t self)
{
	if (self.id)
		free(self.id);

	unsigned int i;
	for (i = 0; i < self.classes_count; i++) {
		if (self.classes[i])
			free(self.classes[i]);
	}
	for (i = 0; i < self.children_count; i++) {
		if (self.children[i].token.type)
			parxml_node_free(self.children[i]);
	}

	free(self.classes);
	free(self.children);
	parxml_token_free(self.token);
	parxml_hashmap_free(&self.attributes);
}

void parxml_node_append_child(
	struct parxml_node_t *self,
	struct parxml_node_t child
)
{
	self->children[self->children_count++] = child;
}

static void parxml_node_print_i(struct parxml_node_t *self, unsigned int i)
{
	unsigned int x = 0;
	while (x++ < i) putchar(' ');

	if (self->token.type == PARXML_TOKENTYPE_TAG_START) {
		printf("<%s", self->token.value);

		/* print id */
		if (self->id)
			printf(" id=\"%s\"", self->id);

		/* print classes */
		if (self->classes_count) {
			printf(" class=\"");
			for (x = 0; x < self->classes_count; x++)
				printf("%s ", self->classes[x]);
			printf("\b\"");
		}

		/* print children */
		for (x = 0; x < self->attributes.count; x++) {
			if (self->attributes.store[x].key) {
				printf(
					" %s=\"%s\"",
					self->attributes.store[x].key,
					self->attributes.store[x].value
				);
			}
		}
		printf(">\n");

		if (self->children[0].token.type != PARXML_TOKENTYPE_TAG_START) {
			x = 0;
			while (x++ < i + 3) putchar(' ');
		}

		for (x = 0; x < self->children_count; x++) {
			if (self->children[x].token.type == PARXML_TOKENTYPE_WORD) {
				parxml_node_print_i(self->children + x, 1);
				if (self->children[x+1].token.type == PARXML_TOKENTYPE_TAG_START)
					putchar('\n');
			} else {
				parxml_node_print_i(self->children + x, i + 4);
				if (self->children[x+1].token.type == PARXML_TOKENTYPE_TAG_START)
					putchar('\n');
			}
		}

		putchar('\n');
		x = 0;
		while (x++ < i) putchar(' ');
		printf("</%s>", self->token.value);
	} else {
		printf("%s", self->token.value);
	}
}

void parxml_node_print(struct parxml_node_t *self)
{
	parxml_node_print_i(self, 0);
	putchar('\n');
}

struct parxml_node_t *parxml_node_search_id(
	struct parxml_node_t *self,
	char *id
)
{
	unsigned int i;
	for (i = 0; i < self->children_count; i++) {
		if (!strcmp(self->children[i].id, id))
			return self->children + i;

		struct parxml_node_t *child_has_id =
			parxml_node_search_id(self->children + i, id);
		if (child_has_id)
			return child_has_id;
	}

	return NULL;
}

static void parxml_node_search_class_i(
	struct parxml_node_t *list,
	struct parxml_node_t *self,
	char *class
)
{
	unsigned int i;
	for (i = 0; i < self->classes_count; i++) {
		if (!strcmp(self->classes[i], class))
			parxml_node_append_child(list, *self);
	}

	for (i = 0; i < self->children_count; i++)
		parxml_node_search_class_i(list, self->children + i, class);
}

struct parxml_node_t parxml_node_search_class(
	struct parxml_node_t *self,
	char *class
)
{
	struct parxml_token_t token = {PARXML_TOKENTYPE_TAG_START, "list"};
	struct parxml_node_t list = parxml_node(token, self->capacity);

	parxml_node_search_class_i(&list, self, class);
	return list;
}

static void parxml_node_search_tag_i(
	struct parxml_node_t *list,
	struct parxml_node_t *self,
	char *tag
)
{
	if (self->token.type == PARXML_TOKENTYPE_TAG_START) {
		if (!strcmp(self->token.value, tag))
			parxml_node_append_child(list, *self);
	}

	unsigned int i;
	for (i = 0; i < self->children_count; i++)
		parxml_node_search_tag_i(list, self->children + i, tag);
}

struct parxml_node_t parxml_node_search_tag(
	struct parxml_node_t *self,
	char *tag
)
{
	struct parxml_token_t token = {PARXML_TOKENTYPE_TAG_START, "list"};
	struct parxml_node_t list = parxml_node(token, self->capacity);

	parxml_node_search_tag_i(&list, self, tag);
	return list;
}

bool parxml_node_has_class(
	struct parxml_node_t *self,
	char *class
)
{
	unsigned int i;
	for (i = 0; i < self->classes_count; i++) {
		if (!strcmp(self->classes[i], class))
			return true;
	}

	return false;
}

void parxml_node_add_class(
	struct parxml_node_t *self,
	char *class
)
{
	bool already_have = parxml_node_has_class(self, class);
	if (already_have)
		return;

	unsigned int i;
	for (i = 0; i < self->classes_count; i++) {
		if (self->classes[i] == NULL) {
			self->classes[i] = strdup(class);
			return;
		}
	}

	self->classes[self->classes_count++] = strdup(class);
}

void parxml_node_remove_class(
	struct parxml_node_t *self,
	char *class
)
{
	unsigned int i;
	for (i = 0; i < self->classes_count; i++) {
		if (!strcmp(self->classes[i], class)) {
			free(self->classes[i]);
			return;
		}
	}
}

void parxml_node_toggle_class(
	struct parxml_node_t *self,
	char *class
)
{
	bool already_have = parxml_node_has_class(self, class);
	if (already_have)
		parxml_node_remove_class(self, class);
	else
		parxml_node_add_class(self, class);
}

struct parxml_parser_t parxml_parser(char *source, unsigned int length)
{
	return (struct parxml_parser_t){
		.length = length,
		.source = source,
		.iter   = source
	};
}

void parxml_parser_free(struct parxml_parser_t parser)
{
	free(parser.source);
}

struct parxml_token_t parxml_parser_next_token(struct parxml_parser_t *parser)
{
	struct parxml_token_t token = {PARXML_TOKENTYPE_ENDOFFILE};
	char c = *parser->iter++;

	while (isspace(c))
		c = *parser->iter++;

	if (c == '<') {
		if (*parser->iter == '/') {
			token.type  = PARXML_TOKENTYPE_LESSERTHAN_SLASH;
			token.value = "</";
			parser->iter++;
		} else {
			token.type  = PARXML_TOKENTYPE_LESSERTHAN;
			token.value = "<";
		}
	} else if (c == '>') {
		token.type  = PARXML_TOKENTYPE_GREATERTHAN;
		token.value = ">";
	} else if (c == '=') {
		token.type  = PARXML_TOKENTYPE_EQUAL;
		token.value = "=";
	} else if (c == '\"' || c == '\'') {
		char pair = c;
		c = *parser->iter++;

		token.type  = PARXML_TOKENTYPE_STRING;
		token.value = malloc(parser->length);
		unsigned int i = 0;

		while (c != pair) {
			token.value[i++] = c;
			c = *parser->iter++;
		}
		token.value[i] = '\0';
	} else if (isalpha(c) || isdigit(c)) {
		token.type  = PARXML_TOKENTYPE_WORD;
		token.value = malloc(parser->length);
		unsigned int i = 0;

		while (!isspace(c) && c != '=' && c != '<' && c != '>') {
			token.value[i++] = c;
			c = *parser->iter++;
		}
		token.value[i] = '\0';
		parser->iter--;
	}

	return token;
}

struct parxml_token_t parxml_parser_peek_token(struct parxml_parser_t *parser)
{
	char *state = parser->iter;
	struct parxml_token_t token = parxml_parser_next_token(parser);
	parser->iter = state;
	return token;
}

struct parxml_node_t parxml_parser_next_node(struct parxml_parser_t *parser)
{
	struct parxml_token_t token = parxml_parser_next_token(parser);

	if (token.type == PARXML_TOKENTYPE_LESSERTHAN) {
		return parxml_parser_parse_tag(parser);
	} else if (token.type == PARXML_TOKENTYPE_LESSERTHAN_SLASH) {
		struct parxml_token_t tag_name;
		tag_name = parxml_parser_next_token(parser);
		assert(tag_name.type == PARXML_TOKENTYPE_WORD);

		struct parxml_token_t next_token;
		next_token = parxml_parser_next_token(parser);
		assert(next_token.type == PARXML_TOKENTYPE_GREATERTHAN);

		struct parxml_node_t ret;
		tag_name.type = PARXML_TOKENTYPE_TAG_END;
		ret = parxml_node(tag_name, 0);
		return ret;
	}

	return parxml_node(token, 0);
}

static void parxml_parse_classes(
	struct parxml_node_t *self,
	struct parxml_token_t *source
)
{
	char *iter = source->value;
	char *buf, *buf_dump;
	buf_dump = buf = malloc(strlen(iter));

	while (*iter != '\0') {
		if (*iter == ' ') {
			*buf = '\0';
			parxml_node_add_class(self, strdup(buf_dump));
			buf = buf_dump;

			while (isspace(*iter)) iter++;
		}
		else {
			*buf++ = *iter++;
		}
	}

	*buf = '\0';
	parxml_node_add_class(self, strdup(buf_dump));
	free(buf_dump);
}

struct parxml_node_t parxml_parser_parse_tag(struct parxml_parser_t *parser)
{
	struct parxml_token_t tag_name = parxml_parser_next_token(parser);
	assert(tag_name.type == PARXML_TOKENTYPE_WORD);
	tag_name.type = PARXML_TOKENTYPE_TAG_START;

	struct parxml_node_t ret = parxml_node(tag_name, parser->length);
	struct parxml_token_t next_token = parxml_parser_next_token(parser);

	while (next_token.type != PARXML_TOKENTYPE_GREATERTHAN) {
		struct parxml_token_t attribute_name;
		attribute_name = next_token;
		assert(attribute_name.type == PARXML_TOKENTYPE_WORD);

		next_token = parxml_parser_peek_token(parser);
		if (next_token.type == PARXML_TOKENTYPE_EQUAL) {
			next_token = parxml_parser_next_token(parser);
			next_token = parxml_parser_next_token(parser);
			assert(next_token.type == PARXML_TOKENTYPE_STRING);

			if (!strcmp(attribute_name.value, "id"))
				ret.id = strdup(next_token.value);
			else if (!strcmp(attribute_name.value, "class"))
				parxml_parse_classes(&ret, &next_token);
			else
				parxml_hashmap_set(
					&ret.attributes,
					attribute_name.value,
					next_token.value
				);

			free(attribute_name.value);
			free(next_token.value);
		} else {
			parxml_hashmap_set(
				&ret.attributes,
				attribute_name.value,
				""
			);
			free(attribute_name.value);
		}

		next_token = parxml_parser_next_token(parser);
	}

	struct parxml_node_t next_node;
	next_node = parxml_parser_next_node(parser);

	while (next_node.token.type) {
		if (parxml_token_match(tag_name, next_node.token))
			break;

		parxml_node_append_child(&ret, next_node);
		next_node = parxml_parser_next_node(parser);
	}

	return ret;
}